Rails.application.routes.draw do
  resources :bugs, except: [:edit]
  resources :projects
  devise_for :users, except: [:edit]
  
  root 'static_pages#home'
  get '/users', to: 'static_pages#home'
  

end
