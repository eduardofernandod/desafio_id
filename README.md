Projeto destinado a seleção de estágio

Objetivo: desenvolvimento de um sistema em uma semana, com tecnologias desconhecidas pelo candidato.

Criação de um sistema de cadastro de projetos e bugs, com sistema de login, de modo que cada bug notificado ou corrigido seja notificado num canal do Slack.

Tecnologias utilizadas:
Ruby on Rails
CoffeeScript
Sass
Slim
Bootstrap


Link: https://young-citadel-80601.herokuapp.com/