class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false, default: ""
      t.string :project_manager, null: false, default: ""
      t.text :description
      t.date :date_create, null: false
      t.date :date_finished
      t.string :area, null: false
      t.string :state, null: false

      t.timestamps
    end
  end
end
