class CreateBugs < ActiveRecord::Migration[5.0]
  def change
    create_table :bugs do |t|
      t.string :title, null: false, default: ""
      t.text :description, null: false, default: ""
      t.boolean :solved, default: false
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
