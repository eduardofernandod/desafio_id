require 'slack-notifier'	

module BugsHelper
  
	CHANNEL = "https://hooks.slack.com/services/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"
	#CHANNEL ="https://hooks.slack.com/services/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"
	def BugsHelper.sendMessage(text, userName)
		notifier = Slack::Notifier.new CHANNEL
		notifier.ping(text, username: userName, icon_emoji: ':japanese_goblin:')
	end

end
