json.array!(@bugs) do |bug|
  json.extract! bug, :id, :title, :description, :solved, :project_id
  json.url bug_url(bug, format: :json)
end
