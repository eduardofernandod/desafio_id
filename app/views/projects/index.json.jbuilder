json.array!(@projects) do |project|
  json.extract! project, :id, :name, :project_manager, :description, :date_create, :date_finished, :area, :state
  json.url project_url(project, format: :json)
end
