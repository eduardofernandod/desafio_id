class Project < ApplicationRecord
  has_many :bugs
  
  validates :name,  presence: true, length: { maximum: 50 }
  validates :project_manager, presence: true, length: { maximum: 50 }
  validates :description, length: { maximum: 300 }
  validates :date_create, presence: true
  validates :area, presence: true
  validates :state, presence: true
  
end
